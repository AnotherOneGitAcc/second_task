# Hardhat project

## How to build and run tests
 * Set up the environment (See: https://hardhat.org/tutorial/setting-up-the-environment.html)

 * Clone repository

    ```
    git clone https://gitlab.com/9pahaE6aha/second_task.git
    ```

 * Install hardhat locally

    ```
    npm init --yes
    npm install --save-dev hardhat
    ```

 * Install dependencies

    ```
    npm install --save-dev @nomiclabs/hardhat-ethers ethers @nomiclabs hardhat-waffle ethereum-waffle chai @openzeppelin/contracts
    ```

 * Run tests

    ```
    npx hardhat test
    ```

 * Delpoy locally

    ```
    npx hardhat run scripts/deploy.js
    ```
