//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "./T.sol";

contract NFT is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdCounter;

    using Strings for uint256;

    T private t;
    address private creator;
    mapping(uint256 => string) private _idToUri;

    mapping(uint256 => address) private theBiggestBidAddress;
    mapping(uint256 => uint256) public theBiggestBid;

    constructor(address contractOfT_) ERC721("NFT Collection", "KFC") {
        creator = msg.sender;
        t = T(contractOfT_);
    }

    function mint(address to, bytes memory data) public returns (uint256) {
        require(
            msg.sender == creator,
            "Only creator can mint nft for this system"
        );

        _safeMint(to, _tokenIdCounter.current(), data);
        _idToUri[_tokenIdCounter.current()] = string(data);
        _tokenIdCounter.increment();

        return _tokenIdCounter.current();
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        require(
            _exists(tokenId),
            "ERC721Metadata: URI query for nonexistent token"
        );

        string memory URI = _idToUri[tokenId];
        return bytes(URI).length > 0 ? string(abi.encodePacked(URI)) : "";
    }

    function addOffer(uint256 tokenId, uint256 cost) public returns (bool) {
        require(
            _exists(tokenId),
            "ERC721Metadata: URI query for nonexistent token"
        );
        require(
            msg.sender != this.ownerOf(tokenId),
            "Cannot buy your own token"
        );
        require(
            t.balanceOf(msg.sender) >= cost,
            "Need to offer less or equal number of tokens that you have"
        );
        require(
            cost > theBiggestBid[tokenId],
            "Need to offer more tokens than the Biggest Bid"
        );

        if (t.transferFrom(msg.sender, creator, cost)) {
            t.getAccessToTokensFromNFT(creator, theBiggestBid[tokenId]);

            if (theBiggestBidAddress[tokenId] != address(0)) {
                t.transferFrom(
                    creator,
                    theBiggestBidAddress[tokenId],
                    theBiggestBid[tokenId]
                );
            }

            theBiggestBidAddress[tokenId] = msg.sender;
            theBiggestBid[tokenId] = cost;
        }

        return true;
    }

    function confirmOffer(uint256 tokenId) public returns (bool) {
        require(
            _exists(tokenId),
            "ERC721Metadata: URI query for nonexistent token"
        );
        require(
            this.ownerOf(tokenId) == msg.sender,
            "Only owner of this nft can confirm offer"
        );
        require(
            theBiggestBidAddress[tokenId] != address(0),
            "You can confirm only existing offer"
        );

        t.getAccessToTokensFromNFT(creator, theBiggestBid[tokenId]);
        if (t.transferFrom(creator, msg.sender, theBiggestBid[tokenId])) {
            this.transferFrom(
                msg.sender,
                theBiggestBidAddress[tokenId],
                tokenId
            );
            theBiggestBidAddress[tokenId] = address(0);
            theBiggestBid[tokenId] = 0;
        }

        return true;
    }
}
