//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract T is ERC20 {
    address private _creator;
    address private _minter; // Make this for staking contract
    address private _allower; // Make this for NFT contrtact

    constructor() ERC20("MyToken", "MTN") {
        _creator = msg.sender;
    }

    function setMinterForStaking(address minter) public {
        require(msg.sender == _creator, "Only creator can set minter");
        require(_minter == address(0), "Minter can be set only once");

        _minter = minter;
    }

    function setAllowerForNFT(address allower) public {
        require(msg.sender == _creator, "Only creator can set allower");
        require(_allower == address(0), "Allower can be set only once");
        
        _allower = allower;
    }

    function getAccessToTokensFromNFT(address withTokens, uint256 addedValue)
        public
    {
        require(msg.sender == _allower, "Only allower can call this function");

        _approve(
            withTokens,
            _allower,
            allowance(withTokens, _allower) + addedValue
        );
    }

    function mint(address to, uint256 amount) public {
        require(
            ((msg.sender == _creator) || (msg.sender == _minter)),
            "Only creator or minter can mint new tokens"
        );

        _mint(to, amount);
    }

    function burn(address to, uint256 amount) public {
        require(msg.sender == _creator, "Only creator can burn tokens");

        _burn(to, amount);
    }
}
