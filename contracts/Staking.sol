//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "./T.sol";
import "./NFT.sol";

contract Staking {
    T private t;
    NFT private nft;

    address private minter;
    mapping(uint256 => address) private owner;
    mapping(uint256 => uint256) private timeStaked;

    event startOfStaking(
        address indexed from,
        uint256 time,
        uint256 indexed tokenId
    );
    event stopOfStaking(
        address indexed to,
        uint256 time,
        uint256 indexed tokenId
    );

    constructor(address contractOfT_, address contractOfMyNFT_) {
        minter = msg.sender;
        t = T(contractOfT_);
        nft = NFT(contractOfMyNFT_);
    }

    function startStaking(uint256 idOfNFT_) public {
        nft.transferFrom(msg.sender, minter, idOfNFT_);
        owner[idOfNFT_] = msg.sender;
        timeStaked[idOfNFT_] = block.timestamp;

        emit startOfStaking(msg.sender, block.timestamp, idOfNFT_);
    }

    function stopStaking(uint256 idOfNFT_) public {
        require(
            owner[idOfNFT_] == msg.sender,
            "Only owner can call this function"
        );

        nft.transferFrom(minter, owner[idOfNFT_], idOfNFT_);
        uint256 timeToPay = block.timestamp - timeStaked[idOfNFT_];
        t.mint(owner[idOfNFT_], timeToPay);

        timeStaked[idOfNFT_] = 0;
        owner[idOfNFT_] = address(0);

        emit stopOfStaking(msg.sender, block.timestamp, idOfNFT_);
    }

    function howMuchTokensStaked(uint256 idOfNFT_)
        public
        view
        returns (uint256)
    {
        if (owner[idOfNFT_] != address(0)) {
            return block.timestamp - timeStaked[idOfNFT_];
        } else {
            return 0;
        }
    }
}
