const hre = require("hardhat");

async function main() {

    const deployer = await hre.ethers.getSigners();

    const T = await hre.ethers.getContractFactory("T");
    const NFT = await hre.ethers.getContractFactory("NFT");
    const STAKING = await hre.ethers.getContractFactory("Staking");

    const t = await T.deploy();
    const nft = await NFT.deploy(t.address);
    const staking = await STAKING.deploy(t.address, nft.address);

    await t.deployed();
    await nft.deployed();
    await staking.deployed();

    const setMinter = await t.setMinterForStaking(staking.address);
    const setAllower = await t.setAllowerForNFT(nft.address);
    const setApprovalForAll = await nft.setApprovalForAll(staking.address, true);

    console.log("T deployed to:", t.address);
    console.log("NFT deployed to:", nft.address);
    console.log("Staking deployed to:", staking.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
