const { expect } = require("chai");
const { ethers, network } = require("hardhat");

describe("SmartContract System Tests", function () {
    let Token;
    let NFT;
    let Staking;

    let hardhatToken;
    let hardhatNFT;
    let hardhatStaking;

    let owner;
    let address1;
    let address2;

    // Similar to deploy rules
    this.beforeEach(async function () {
        [owner, address1, address2] = await hre.ethers.getSigners();

        Token = await hre.ethers.getContractFactory("T");
        NFT = await hre.ethers.getContractFactory("NFT");
        Staking = await hre.ethers.getContractFactory("Staking");

        hardhatToken = await Token.deploy();
        hardhatNFT = await NFT.deploy(hardhatToken.address);
        hardhatStaking = await Staking.deploy(hardhatToken.address, hardhatNFT.address);

        await hardhatToken.deployed();
        await hardhatNFT.deployed();
        await hardhatStaking.deployed();

        const setMinter = await hardhatToken.setMinterForStaking(hardhatStaking.address);
        const setAllower = await hardhatToken.setAllowerForNFT(hardhatNFT.address);
        const setApprovalForAll = await hardhatNFT.setApprovalForAll(hardhatStaking.address, true);
    });

    describe("Token transactions", function () {
        it("Should mint tokens to addr1", async function () {
            await hardhatToken.mint(address1.address, 1000);

            const addr1Balance = await hardhatToken.balanceOf(address1.address);
            expect(addr1Balance).to.equal(1000);
        });

        it("Should send half tokens to addr2 from addr1", async function () {
            await hardhatToken.mint(address1.address, 1000);

            await hardhatToken.connect(address1).transfer(address2.address, 500);

            const addr1Balance = await hardhatToken.balanceOf(address1.address);
            const addr2Balance = await hardhatToken.balanceOf(address2.address);

            expect(addr1Balance).to.equal(addr2Balance);
        });

        it("Should burn tokens to owner", async function () {
            await hardhatToken.mint(owner.address, 1000);
            await hardhatToken.burn(owner.address, 500);

            const ownerBalance = await hardhatToken.balanceOf(owner.address);
            expect(ownerBalance).to.equal(500);
        });

    });

    describe("Token failed transactions", function () {
        it("Shouldn't mint tokens to addr1", async function () {
            await expect(
                hardhatToken.connect(address1).mint(address1.address, 1000)
            ).to.be.revertedWith("Only creator or minter can mint new tokens")

        });

        it("Shouldn't send tokens to addr2", async function () {
            await expect(
                hardhatToken.connect(address1).transfer(address2.address, 500)
            ).to.be.revertedWith("ERC20: transfer amount exceeds balance");

        });

        it("Shouldn't burn tokens to owner", async function () {
            await expect(
                hardhatToken.burn(owner.address, 500)
            ).to.be.revertedWith("ERC20: burn amount exceeds balance");
        });
    });

    describe("NFT transactions", function () {
        it("Should mint nft to addr1", async function () {
            await hardhatNFT.mint(address1.address, "0x60");

            const ownerOfNFT = await hardhatNFT.ownerOf(0);
            expect(ownerOfNFT).to.equal(address1.address);
        });

        it("Should return correct token URI", async function () {
            await hardhatNFT.mint(address1.address, "0x48656c6c6f2c2074686973206973206d7920736f6c696469747920746573747321");

            const tokenUri = await hardhatNFT.tokenURI(0);
            expect(tokenUri).to.equal("Hello, this is my solidity tests!");
        });

        it("Should add an offer in tokens to addr1", async function () {
            await hardhatNFT.mint(address1.address, "0x49");
            await hardhatToken.mint(address2.address, 500);

            await hardhatToken.connect(address2).increaseAllowance(hardhatNFT.address, 500);
            await hardhatNFT.connect(address2).addOffer(0, 500);

            const bid = await hardhatNFT.theBiggestBid(0);
            expect(bid).to.equal(500);
        });

        it("Should sell NFT for some tokens", async function () {
            await hardhatNFT.mint(address1.address, "0x60");
            await hardhatToken.mint(address2.address, 500);

            await hardhatToken.connect(address2).increaseAllowance(hardhatNFT.address, 500);
            await hardhatNFT.connect(address2).addOffer(0, 500);

            await hardhatNFT.connect(address1).approve(hardhatNFT.address, 0);
            await hardhatNFT.connect(address1).confirmOffer(0);

            const addressOfOwner = await hardhatNFT.ownerOf(0);
            expect(addressOfOwner).to.equal(address2.address);
        });

        it("Should overbid and sell for new price", async function () {
            await hardhatNFT.mint(owner.address, "0x60");
            await hardhatToken.mint(address2.address, 500);
            await hardhatToken.mint(address1.address, 500);

            await hardhatToken.connect(address1).increaseAllowance(hardhatNFT.address, 250);
            await hardhatNFT.connect(address1).addOffer(0, 250);

            await hardhatToken.connect(address2).increaseAllowance(hardhatNFT.address, 500);
            await hardhatNFT.connect(address2).addOffer(0, 500);

            await hardhatNFT.approve(hardhatNFT.address, 0);
            await hardhatNFT.confirmOffer(0);

            const balanceOfOwner = await hardhatToken.balanceOf(owner.address);
            const balanceOfaddr1 = await hardhatToken.balanceOf(address1.address);

            expect(balanceOfOwner).to.equal(balanceOfaddr1);
        });

        it("Should overbid your own bid", async function () {
            await hardhatNFT.mint(owner.address, "0x60");
            await hardhatToken.mint(address1.address, 750);

            await hardhatToken.connect(address1).increaseAllowance(hardhatNFT.address, 250);
            await hardhatNFT.connect(address1).addOffer(0, 250);

            await hardhatToken.connect(address1).increaseAllowance(hardhatNFT.address, 500);
            await hardhatNFT.connect(address1).addOffer(0, 500);

            const balanceOfaddr1 = await hardhatToken.balanceOf(address1.address);

            expect(balanceOfaddr1).to.equal(250);
        });
    });

    describe("NFT failed transactions", function () {
        it("Shouldn't mint nft to addr1", async function () {
            await expect(
                hardhatNFT.connect(address1).mint(address1.address, "0x60")
            ).to.be.revertedWith("Only creator can mint nft for this system")

        });

        it("Shouldn't add an offer in tokens to addr1", async function () {
            await expect(
                hardhatNFT.addOffer(0, 500)
            ).to.be.revertedWith("ERC721Metadata: URI query for nonexistent token");

        });

        it("Shouldn't sell NFT for some tokens", async function () {
            await hardhatNFT.mint(address1.address, "0x60");
            await hardhatToken.mint(address2.address, 500);

            await hardhatToken.connect(address2).increaseAllowance(hardhatNFT.address, 500);
            await hardhatNFT.connect(address2).addOffer(0, 500);

            await hardhatNFT.connect(address1).approve(hardhatNFT.address, 0);

            await expect(
                hardhatNFT.connect(address2).confirmOffer(0)
            ).to.be.revertedWith("Only owner of this nft can confirm offer");
        });
    });

    describe("Staking", function () {
        it("Should stake some tokens T to NFT Owner Balance", async function () {
            await hardhatNFT.mint(address1.address, "0x60");
            await hardhatNFT.connect(address1).approve(hardhatStaking.address, 0);

            await hardhatStaking.connect(address1).startStaking(0);
            await network.provider.send("evm_increaseTime", [3600]);
            await hardhatStaking.connect(address1).stopStaking(0);

            const balanceOfAddress1 = await hardhatToken.balanceOf(address1.address);
            expect(balanceOfAddress1).to.be.at.least(3600);
        });

        it("Should see how much tokens can be staked", async function () {
            await hardhatNFT.mint(address1.address, "0x60");
            await hardhatNFT.connect(address1).approve(hardhatStaking.address, 0);

            await hardhatStaking.connect(address1).startStaking(0);
            await network.provider.send("evm_increaseTime", [3600]);

            // Make transaction to set new block time
            await hardhatNFT.mint(address2.address, "0x60");

            const alreadyStaked = await hardhatStaking.howMuchTokensStaked(0);
            expect(alreadyStaked).to.be.at.least(3600);
        });
    });
});